package com.ibnu.template.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemplateJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TemplateJavaApplication.class, args);
	}

}
